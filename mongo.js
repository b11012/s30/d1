/*
	Mini-Activity: 5 mins

	>> Create a new database called session30

	>> In session30, create a 5 new documents in a new fruits collection with the following fields and values:

		name: "Apple",
		supplier: "Red Farms Inc."
		stocks: 20
		price: 40
		onSale: true

		name: "Banana",
		supplier: "Yellow Farms"
		stocks: 15
		price: 20
		onSale: true

		name: "Kiwi"
		supplier: "Green Farming and Canning"
		stocks: 25
		price: 50
		onSale: true

		name: "Mango"
		supplier: "Yellow Farms"
		stocks: 10
		price: 60
		onSale: true

		name: "Dragon Fruit"
		supplier: "Red Farms Inc."
		stocks: 10
		price: 60
		onSale: true


	>> Send your screenshots in our Batch Hangouts

*/

// S O L U T I O N

	db.fruits.insertMany([
	{
		name: "Apple",
		supplier: "Red Farms Inc.",
		stocks: 20,
		price: 40,
		onSale: true
	},

	{
		name: "Banana",
		supplier: "Yellow Farms",
		stocks: 15,
		price: 20,
		onSale: true
	},

	{
		name: "Kiwi",
		supplier: "Green Farming and Canning",
		stocks: 25,
		price: 50,
		onSale: true
	},
	{
		name: "Mango",
		supplier: "Yellow Farms",
		stocks: 10,
		price: 60,
		onSale: true
	},

	{
		name: "Dragon Fruit",
		supplier: "Red Farms Inc.",
		stocks: 10,
		price: 60,
		onSale: true
	}
	])

// Aggregation in MongoDB
	// process of generating manipulated data and perform operations to create filtered results that helps in data analysis
	// this helps in creating reports from analyzing data provided in our documents

	// Aggregation Pipelines
		// aggregation is done in 2 to 3 steps

		// $match- checks the documents that will pass the condition
			// Syntax: {$match: {existingField: value}}

		// $group- grouped documents together to create analysis of these grouped documents
			// Syntax: {$group: {_id: <id or existingField>, fieldResultName: "valueResult"}}

		// $sum- get the total of all values in the given field


	db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", totalStocks: {$sum: "stocks"}}
		}
	])

	db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", totalPrice: {$sum: "$price"}}
		}
    ])

    db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", nonNumerical: {$sum: "$name"}}
		}
    ])

    // returns 0


    /*
    	Mini-Activity: 5 mins

    	>> In a new aggregation for fruits collection,

    		>> First, match all items with supplier as Red Farms Inc.

    		>> Second, group the matched documents in a single aggregated result of the total stocks of items supplied by Red Farms Inc.

    		>> Result should be the total number of stocks for items supplied by Red Farms Inc.

    		>> Send your screenshots in our Batch Hangouts
    */

  // S O L U T I O N

  db.fruits.aggregate([
  		{
  			$match: {supplier: "Red Farms Inc."}
  		},
  		{
  			$group: {_id: "notForSale", total: {$sum: "$stocks"}}
  		}
  	])

  // average price of all items on sale ($avg)

  db.fruits.aggregate([
  	{
  		$match: {onSale: true}
  	},
  	{
  		$group: {_id: "avgPriceOnSale", avgPrice: {$avg: "$price"}}
  	}
  ])


  // multiply operator

  db.fruits.aggregate([
  		{
  			$match: {onSale: true}
  		},
  		{
  			$project : {supplier: 1, _id: 0, 
  				inventory: {$multiply: ["$price", "$stocks"]}}
  		}

  	])

  // $max- highest value out of all the values in the given field

  db.fruits.aggregate([
  		{
  			$match: {onSale: true}
  		},
  		{
  			$group: {_id: "maxStockOnSale", maxStock: {$max: "$stocks"}}
  		}
  	])

  // $min- get the lowest values in the given field
  db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "minStockForSale", minStock: {$min: "$stocks"}}
        }
])    

  // $count- counting the items that matches the match stage

  db.fruits.aggregate([
  		{
  			$match: {onSale: true}
  		},
  		{
  			$count: "itemsOnSale"
  		}

  	])

   db.fruits.aggregate([
        {
                $match: {price: {$lt: 50}}
        },
        {
                $count: "priceIsLessThan50"
        }

])  

// count total number of items per group

	db.fruits.aggregate([
			{
				$match: {onSale: true}
			},
			{
				$group: {_id: "$supplier", noOfItems: {$sum: 1}}
			}

		])

	db.fruits.aggregate([
	        {
                $match: {onSale: true}
	        },
	        {
                $group: {_id: "$supplier", noOfItems: {$sum: 1}, totalStocks: {$sum: "$stocks"}}
	        }

	])

	db.fruits.aggregate([
			{
				$match: {onSale: true}
			},
			{
				$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
			},
			{
				$out: "totalStocksPerSupplier"
			}

	])

